/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/23 18:54:13 by apyvovar          #+#    #+#             */
/*   Updated: 2018/09/23 18:54:16 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"
#include "events.h"
#include "draw_tools.h"
#include "set_mandelbrot.h"
#include "set_julia.h"
#include "set_ship_five_partial.h"
#include "set_celtic_five_mbar.h"
#include "set_mandelbar.h"

static void					init(t_win *win, enum e_currentset index)
{
	int	bits_pp;
	int	size_line;
	int	endian;

	if (!(win->mlx = mlx_init()))
		manage_exit(EXIT_FAILURE, "Unable to init mlx\n");
	if (!(win->mlx_win = mlx_new_window(win->mlx, WIN_W, WIN_H, TITLE)))
		manage_exit(EXIT_FAILURE, "Unable to init mlx window\n");
	if (!(win->mlx_img = mlx_new_image(win->mlx, WIN_W, WIN_H)))
		manage_exit(EXIT_FAILURE, "Unable to init mlx image\n");
	win->mlx_img_addr = mlx_get_data_addr(win->mlx_img, &bits_pp, \
	&size_line, &endian);
	main_palette_init(win);
	win->palette = PALETTE_A;
	win->current_set = NULL;
	init_mandelbrot(&win->sets[MANDELBROT]);
	init_julia(&win->sets[JULIA]);
	win->julia_mouse_flag = 1;
	init_ship_5_partial(&win->sets[SHIP5PARTIAL]);
	init_celtic_5mbar(&win->sets[CELTIC5MBAR]);
	init_mandelbar(&win->sets[MANDELBAR]);
	win->show_menu = 0;
	set_current_set(win, index);
}

static enum e_currentset	resolve_params(int argc, const char **argv)
{
	int	index;

	if (argc != 2 || ft_strlen(argv[1]) > 1 || !ft_isdigit(argv[1][0]))
		exit(manage_exit(EXIT_FAILURE, NULL));
	index = argv[1][0] - '0';
	if (index == 0 || index > SET_COUNT)
		exit(manage_exit(EXIT_FAILURE, NULL));
	if (index == 1)
		return (MANDELBROT);
	if (index == 2)
		return (JULIA);
	if (index == 3)
		return (SHIP5PARTIAL);
	if (index == 4)
		return (CELTIC5MBAR);
	if (index == 5)
		return (MANDELBAR);
	manage_exit(EXIT_FAILURE, "unmanaged parameter\n");
	return (MANDELBROT);
}

int							main(int argc, const char *argv[])
{
	t_win	win;

	init(&win, resolve_params(argc, argv));
	mlx_hook(win.mlx_win, KEYPRESSNOTIFY, 0, manage_key_event, (void *)&win);
	mlx_hook(win.mlx_win, DESTROYNOTIFY, 0, manage_exit, NULL);
	mlx_hook(win.mlx_win, MOTIONNOTIFY, 0, manage_julia_event, (void *)&win);
	mlx_mouse_hook(win.mlx_win, manage_mouse_event, (void *)&win);
	mlx_loop(win.mlx);
	return (0);
}

int							manage_exit(int is_error, const char *errmsg)
{
	if (!is_error)
		exit(is_error);
	if (errmsg)
		fmt_print((char *)errmsg, 10, 2, 0);
	else
	{
		ft_putstr("Usage: ./fractol [initial set]\n\n");
		ft_putstr("    Initial set - number from 1 to 5\n");
		ft_putstr("    1 - Mandelbrot set\n");
		ft_putstr("    2 - Julia set\n");
		ft_putstr("    3 - Burning ship set\n");
		ft_putstr("    4 - Celtic set\n");
		ft_putstr("    5 - Mandelbar set\n");
	}
	exit(is_error);
}
