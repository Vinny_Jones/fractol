/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_julia.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 13:28:21 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 13:28:23 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_julia.h"

void	init_julia(t_set *set)
{
	ft_strcpy(set->name, "JULIA SET");
	set->draw = draw_julia;
	set->re_lim = 1.5;
	set->im_lim = 1;
	set->shift = make_dpair(0, 0);
	set->c = make_dpair(-0.7, 0.257);
	set->zoom = 1;
}

void	*draw_julia(void *param)
{
	t_thread_param	*p;
	t_ipair			coords;
	t_dpair			helper[2];

	p = (t_thread_param *)param;
	while (p->current < p->end)
	{
		coords.x = p->current % WIN_W;
		coords.y = p->current / WIN_W;
		helper[1].x = 0;
		helper[1].y = 0;
		helper[0].x = p->set->shift.x + p->set->re_lim * \
		(coords.x - 0.5 * WIN_W) / (0.5 * WIN_W * p->set->zoom);
		helper[0].y = p->set->shift.y + p->set->im_lim * \
		(coords.y - 0.5 * WIN_H) / (0.5 * WIN_H * p->set->zoom);
		p->current++;
		draw_pixel(p->win, coords.x, coords.y, iterations(&p->set->c, helper));
	}
	return (NULL);
}

void	manage_julia_keys(t_win *win, int key_code)
{
	if (win->current_set != &win->sets[JULIA])
		return ;
	if (key_code == C_RE_ADD)
		win->current_set->c.x += (JULIA_C_STEP / win->current_set->zoom);
	else if (key_code == C_RE_SUB)
		win->current_set->c.x -= (JULIA_C_STEP / win->current_set->zoom);
	else if (key_code == C_IM_ADD)
		win->current_set->c.y += (JULIA_C_STEP / win->current_set->zoom);
	else if (key_code == C_IM_SUB)
		win->current_set->c.y -= (JULIA_C_STEP / win->current_set->zoom);
	else if (key_code == C_MOUSE)
		win->julia_mouse_flag = !win->julia_mouse_flag;
	else
		return ;
	draw_current_set(win);
}
