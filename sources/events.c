/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/23 19:15:08 by apyvovar          #+#    #+#             */
/*   Updated: 2018/09/23 19:15:08 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "events.h"

static void	manage_zoom(t_win *win, int zoom, int x, int y)
{
	t_set	*set;
	t_dpair	delta;

	set = win->current_set;
	delta.x = set->re_lim * (x - 0.5 * WIN_W) / (0.5 * WIN_W * set->zoom);
	delta.y = set->im_lim * (y - 0.5 * WIN_H) / (0.5 * WIN_H * set->zoom);
	set->zoom *= (zoom == ZOOM_IN ? 1.1 : 0.9);
	delta.x -= set->re_lim * (x - 0.5 * WIN_W) / (0.5 * WIN_W * set->zoom);
	delta.y -= set->im_lim * (y - 0.5 * WIN_H) / (0.5 * WIN_H * set->zoom);
	set->shift.x += delta.x;
	set->shift.y += delta.y;
	draw_current_set(win);
}

static void	manage_control_keys(t_win *win, int key_code)
{
	t_set	*set;

	set = win->current_set;
	if (key_code == ESC_KEY)
		manage_exit(EXIT_SUCCESS, NULL);
	if (key_code == PALETTE_A_KEY && win->palette != PALETTE_A)
		win->palette = PALETTE_A;
	else if (key_code == PALETTE_B_KEY && win->palette != PALETTE_B)
		win->palette = PALETTE_B;
	else if (key_code == PALETTE_C_KEY && win->palette != PALETTE_C)
		win->palette = PALETTE_C;
	else if (key_code == UP_KEY)
		set->shift.y -= set->im_lim * 2 / set->zoom * 0.05;
	else if (key_code == DOWN_KEY)
		set->shift.y += set->im_lim * 2 / set->zoom * 0.05;
	else if (key_code == LEFT_KEY)
		set->shift.x -= set->re_lim * 2 / set->zoom * 0.05;
	else if (key_code == RIGHT_KEY)
		set->shift.x += set->re_lim * 2 / set->zoom * 0.05;
	else if (key_code == MENU_KEY)
		win->show_menu = !win->show_menu;
	else
		return ;
	draw_current_set(win);
}

int			manage_key_event(int key_code, void *param)
{
	t_win	*win;

	win = (t_win*)param;
	manage_control_keys(win, key_code);
	if (key_code == PLUS_KEY1 || key_code == PLUS_KEY2)
		manage_zoom(win, ZOOM_IN, WIN_W / 2, WIN_H / 2);
	else if (key_code == MINUS_KEY1 || key_code == MINUS_KEY2)
		manage_zoom(win, ZOOM_OUT, WIN_W / 2, WIN_H / 2);
	else if (key_code == SET_1)
		set_current_set(win, MANDELBROT);
	else if (key_code == SET_2)
		set_current_set(win, JULIA);
	else if (key_code == SET_3)
		set_current_set(win, SHIP5PARTIAL);
	else if (key_code == SET_4)
		set_current_set(win, CELTIC5MBAR);
	else if (key_code == SET_5)
		set_current_set(win, MANDELBAR);
	manage_julia_keys(win, key_code);
	return (0);
}

int			manage_mouse_event(int key_kode, int x, int y, void *param)
{
	t_win	*win;

	win = (t_win*)param;
	if (key_kode == ZOOM_IN_KEY || key_kode == ZOOM_OUT_KEY)
		manage_zoom(win, (key_kode == ZOOM_IN_KEY) ? ZOOM_IN : ZOOM_OUT, x, y);
	return (0);
}

int			manage_julia_event(int x, int y, void *param)
{
	t_win	*win;
	t_set	*julia;

	win = (t_win*)param;
	if (win->current_set != &win->sets[JULIA] || !win->julia_mouse_flag)
		return (0);
	if (x < 0 || y < 0 || x >= WIN_W || y >= WIN_H)
		return (0);
	julia = &win->sets[JULIA];
	julia->c.x = julia->shift.x + julia->re_lim * \
	(x - 0.5 * WIN_W) / (0.5 * WIN_W * julia->zoom);
	julia->c.y = julia->shift.y + julia->im_lim * \
	(y - 0.5 * WIN_H) / (0.5 * WIN_H * julia->zoom);
	draw_current_set(win);
	return (0);
}
