/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_celtic_five_mbar.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 13:28:09 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 13:28:14 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_celtic_five_mbar.h"

static int	get_iterations(t_dpair *z, t_dpair *c)
{
	int	n;

	n = 0;
	while (n < MAX_ITER)
	{
		z[1] = z[0];
		z[0].x = fabs(z[1].x * (z[1].x * z[1].x * z[1].x * z[1].x - \
		10 * z[1].x * z[1].x * z[1].y * z[1].y + \
		5 * z[1].y * z[1].y * z[1].y * z[1].y)) + c->x;
		z[0].y = -z[1].y * (5 * z[1].x * z[1].x * z[1].x * z[1].x - \
		10 * z[1].x * z[1].x * z[1].y * z[1].y + \
		z[1].y * z[1].y * z[1].y * z[1].y) + c->y;
		if (z[0].x * z[0].x + z[0].y * z[0].y > 4)
			break ;
		n++;
	}
	return (n);
}

void		init_celtic_5mbar(t_set *set)
{
	ft_strcpy(set->name, "CELTIC 5TH MBAR SET");
	set->draw = draw_celtic_5mbar;
	set->re_lim = 1.5;
	set->im_lim = 1;
	set->shift = make_dpair(-0.3, 0);
	set->c = make_dpair(0, 0);
	set->zoom = 0.85;
}

void		*draw_celtic_5mbar(void *input)
{
	t_thread_param	*p;
	t_ipair			coords;
	t_dpair			params;
	t_dpair			helper[2];

	p = (t_thread_param *)input;
	while (p->current < p->end)
	{
		coords.x = p->current % WIN_W;
		coords.y = p->current / WIN_W;
		params.x = p->set->shift.x + p->set->re_lim * \
		(coords.x - 0.5 * WIN_W) / (0.5 * WIN_W * p->set->zoom);
		params.y = p->set->shift.y + p->set->im_lim * \
		(coords.y - 0.5 * WIN_H) / (0.5 * WIN_H * p->set->zoom);
		helper[0].x = 0;
		helper[0].y = 0;
		helper[1].x = 0;
		helper[1].y = 0;
		p->current++;
		draw_pixel(p->win, coords.x, coords.y, get_iterations(helper, &params));
	}
	return (NULL);
}
