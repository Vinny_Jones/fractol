/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_mandelbrot.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 13:28:40 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 13:28:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "set_mandelbrot.h"

void	init_mandelbrot(t_set *set)
{
	ft_strcpy(set->name, "MANDELBROT SET");
	set->draw = draw_mandelbrot;
	set->re_lim = 1.5;
	set->im_lim = 1;
	set->shift = make_dpair(-0.5, 0);
	set->c = make_dpair(0, 0);
	set->zoom = 1;
}

void	*draw_mandelbrot(void *input)
{
	t_thread_param	*p;
	t_ipair			coords;
	t_dpair			c;
	t_dpair			helper[2];

	p = (t_thread_param *)input;
	while (p->current < p->end)
	{
		coords.x = p->current % WIN_W;
		coords.y = p->current / WIN_W;
		c.x = p->set->shift.x + p->set->re_lim * \
		(coords.x - 0.5 * WIN_W) / (0.5 * WIN_W * p->set->zoom);
		c.y = p->set->shift.y + p->set->im_lim * \
		(coords.y - 0.5 * WIN_H) / (0.5 * WIN_H * p->set->zoom);
		helper[0].x = 0;
		helper[0].y = 0;
		helper[1].x = 0;
		helper[1].y = 0;
		p->current++;
		draw_pixel(p->win, coords.x, coords.y, iterations(&c, helper));
	}
	return (NULL);
}
