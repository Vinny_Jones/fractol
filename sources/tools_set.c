/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_set.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 13:28:58 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 13:29:00 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tools_set.h"

static void	draw_menu(t_win *win)
{
	void	*mlx;
	void	*w;

	mlx = win->mlx;
	w = win->mlx_win;
	if (!win->show_menu)
		mlx_string_put(mlx, w, 10, 10, 0xFFFFFF, STR_SHOWMENU);
	else
	{
		mlx_string_put(mlx, w, 10, 10, 0xFFFFFF, STR_HIDEMENU);
		mlx_string_put(mlx, w, 10, 30, 0xFFFFFF, STR_CHANGESET);
		mlx_string_put(mlx, w, 10, 50, 0xFFFFFF, STR_CHANGEPALETTE);
		mlx_string_put(mlx, w, 10, 70, 0xFFFFFF, STR_ARROWS);
		mlx_string_put(mlx, w, 10, 90, 0xFFFFFF, STR_ZOOM);
		if (win->current_set == &win->sets[JULIA])
		{
			mlx_string_put(mlx, w, 10, 110, 0xFFFFFF, STR_JULIAOPTIONS);
			mlx_string_put(mlx, w, 10, 130, 0xFFFFFF, STR_JULIAMOUSE);
			if (win->julia_mouse_flag)
				mlx_string_put(mlx, w, 330, 130, 0x00FF00, "ON");
			else
				mlx_string_put(mlx, w, 330, 130, 0xFF0000, "OFF");
		}
	}
	mlx_string_put(mlx, w, 10, WIN_H - 30, 0x00FF00, win->current_set->name);
}

void		draw_current_set(t_win *win)
{
	t_thread_param	opts[MAX_THR];
	int				i;

	mlx_clear_window(win->mlx, win->mlx_win);
	i = -1;
	while (++i < MAX_THR)
	{
		opts[i].win = win;
		opts[i].set = win->current_set;
		opts[i].current = i * PPTHR;
		opts[i].end = (i == MAX_THR - 1 ? WIN_W * WIN_H : (i + 1) * PPTHR);
		pthread_create(&opts[i].t_id, NULL, win->current_set->draw, &opts[i]);
	}
	i = -1;
	while (++i < MAX_THR)
		pthread_join(opts[i].t_id, NULL);
	mlx_put_image_to_window(win->mlx, win->mlx_win, win->mlx_img, 0, 0);
	draw_menu(win);
}

inline void	set_current_set(t_win *win, enum e_currentset index)
{
	if (win->current_set == &win->sets[index])
		return ;
	win->current_set = &win->sets[index];
	draw_current_set(win);
}

/*
** h param - helper, used for calculations. h[0] - current set, h[1] - previous.
** x - re, y - im.
*/

int			iterations(t_dpair *c, t_dpair *h)
{
	int	n;

	n = 0;
	while (n < MAX_ITER)
	{
		h[1] = h[0];
		h[0].x = h[1].x * h[1].x - h[1].y * h[1].y + c->x;
		h[0].y = 2 * h[1].x * h[1].y + c->y;
		if (h[0].x * h[0].x + h[0].y * h[0].y > 4)
			break ;
		n++;
	}
	return (n);
}
