/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 13:27:52 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 13:27:54 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "draw_tools.h"

static int	main_palette(t_win *win, int iterations)
{
	if (iterations == MAX_ITER)
		return (CLR_BLACK);
	return (win->main_palette_map[iterations % MAINPALETTE_COUNT]);
}

static int	palette_b(int iterations)
{
	if (iterations == MAX_ITER)
		return (CLR_BLACK);
	return (CLR_WHITE - iterations * CLR_WHITE / MAX_ITER);
}

static int	palette_c(int iterations)
{
	int		color;
	char	*color_ptr;

	if (iterations == MAX_ITER)
		return (CLR_BLACK);
	color = 0;
	color_ptr = (char *)&color;
	*color_ptr++ = (char)(iterations % 256);
	*color_ptr++ = (char)(iterations % 256);
	*color_ptr = (char)(255 - iterations % 256);
	return (color);
}

void		draw_pixel(t_win *win, int x, int y, int iterations)
{
	int	*img;
	int	color;

	if (x < 0 || y < 0 || x >= WIN_W || y >= WIN_H)
		return ;
	if (iterations < 0)
		iterations = CLR_BLACK;
	color = CLR_WHITE;
	if (win->palette == PALETTE_A)
		color = main_palette(win, iterations);
	else if (win->palette == PALETTE_B)
		color = palette_b(iterations);
	else if (win->palette == PALETTE_C)
		color = palette_c(iterations);
	img = (int*)win->mlx_img_addr + y * WIN_W + x;
	*img = color;
}

void		main_palette_init(t_win *win)
{
	win->main_palette_map[0] = 0x421e0f;
	win->main_palette_map[1] = 0x19071a;
	win->main_palette_map[2] = 0x09012f;
	win->main_palette_map[3] = 0x040449;
	win->main_palette_map[4] = 0x000764;
	win->main_palette_map[5] = 0x0c2c8a;
	win->main_palette_map[6] = 0x1852b1;
	win->main_palette_map[7] = 0x397dd1;
	win->main_palette_map[8] = 0x86b5e5;
	win->main_palette_map[9] = 0xd3ecf8;
	win->main_palette_map[10] = 0xf1e9bf;
	win->main_palette_map[11] = 0xf8c95f;
	win->main_palette_map[12] = 0xffaa00;
	win->main_palette_map[13] = 0xcc8000;
	win->main_palette_map[14] = 0x995700;
	win->main_palette_map[15] = 0x6a3403;
}
