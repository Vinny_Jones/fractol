# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 14:40:31 by apyvovar          #+#    #+#              #
#    Updated: 2017/03/12 12:08:30 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#########################     Basic     ########################################
CCFLAGS = -Wall -Wextra -Werror
LFLAGS = -framework OpenGL -framework AppKit
CC = gcc $(CCFLAGS)
LIBDIR = libft
MLXDIR = MinilibX
LIB = $(LIBDIR)/libft.a
LIBMLX = $(MLXDIR)/libmlx.a
#########################     Filler    ########################################
NAME = fractol
INC = includes
SRC_DIR = sources/
SRC_FILES = main.c \
			events.c \
			draw_tools.c \
			tools_set.c \
			set_mandelbrot.c \
			set_mandelbar.c \
			set_julia.c \
			set_ship_five_partial.c \
			set_celtic_five_mbar.c

SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	make -C $(LIBDIR)
	make -C $(MLXDIR)
	$(CC) $(OBJ) $(LIB) $(LIBMLX) $(LFLAGS) -lm -o $@

.c.o: $(SRC)
	$(CC) -I $(INC) -I $(LIBDIR)/$(INC) -I $(MLXDIR)/sierra -c $^ -o $@

clean:
	make -C $(LIBDIR) clean
	make -C $(MLXDIR) clean
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm -f $(LIB)
	/bin/rm -f $(LIBMLX)
	/bin/rm -f $(NAME)

re: fclean all
