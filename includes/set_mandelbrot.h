/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_mandelbrot.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:53:03 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:53:05 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_MANDELBROT_H
# define SET_MANDELBROT_H

# include "main.h"
# include "draw_tools.h"
# include "tools_set.h"

void	init_mandelbrot(t_set *set);
void	*draw_mandelbrot(void *input);

#endif
