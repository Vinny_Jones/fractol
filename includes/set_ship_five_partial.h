/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_ship_five_partial.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:53:15 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:53:17 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_SHIP_FIVE_PARTIAL_H
# define SET_SHIP_FIVE_PARTIAL_H

# include "main.h"
# include "draw_tools.h"
# include "tools_set.h"

void	init_ship_5_partial(t_set *set);
void	*draw_ship_5_partial(void *input);

#endif
