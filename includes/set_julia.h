/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_julia.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:52:37 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:52:39 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_JULIA_H
# define SET_JULIA_H

# include "main.h"
# include "draw_tools.h"
# include "tools_set.h"

# define C_RE_ADD 12
# define C_RE_SUB 0
# define C_IM_ADD 13
# define C_IM_SUB 1
# define C_MOUSE 14
# define JULIA_C_STEP 0.0005

void	init_julia(t_set *set);
void	*draw_julia(void *param);
void	manage_julia_keys(t_win *win, int key_code);

#endif
