/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:52:09 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:52:13 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include "libft.h"
# include "mlx.h"
# include "pthread.h"
# include <math.h>

# define TITLE "fract'ol by apyvovar"
# define WIN_W 800
# define WIN_H 600
# define MAX_ITER 512
# define NAME_BUFF 50

/*
** Number of threads to be used
*/

# define MAX_THR 10

/*
** Number of pixels to be resolved in one thread
*/

# define PPTHR (WIN_H * WIN_W / MAX_THR)

/*
** Event names got from X11/x.h
*/

# define KEYPRESSNOTIFY 2
# define DESTROYNOTIFY 17
# define MOTIONNOTIFY 6

# define MAINPALETTE_COUNT 16

/*
** Set enum and count
*/
# define SET_COUNT 5

enum	e_currentset {
	MANDELBROT = 0,
	JULIA = 1,
	SHIP5PARTIAL = 2,
	CELTIC5MBAR = 3,
	MANDELBAR = 4
};

typedef struct	s_set {
	void	*(*draw)(void * param);
	char	name[NAME_BUFF];
	double	re_lim;
	double	im_lim;
	t_dpair	c;
	t_dpair	shift;
	double	zoom;
}				t_set;

typedef struct	s_win {
	void	*mlx;
	void	*mlx_win;
	void	*mlx_img;
	char	*mlx_img_addr;
	t_set	sets[SET_COUNT];
	t_set	*current_set;
	int		main_palette_map[MAINPALETTE_COUNT];
	int		julia_mouse_flag;
	int		show_menu;
	int		palette;
}				t_win;

typedef struct	s_thread_param {
	t_win		*win;
	t_set		*set;
	int			current;
	int			end;
	pthread_t	t_id;
}				t_thread_param;

int				manage_exit(int is_error, const char *errmsg);

#endif
