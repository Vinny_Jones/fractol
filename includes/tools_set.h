/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_set.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:53:25 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:53:26 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOOLS_SET_H
# define TOOLS_SET_H

# include "main.h"

# define STR_SHOWMENU "Press M to show controls"
# define STR_HIDEMENU "Press M to hide controls"
# define STR_CHANGESET "Main numbers: change set"
# define STR_CHANGEPALETTE "NumPad numbers: change palette"
# define STR_ARROWS "Arrows: shift"
# define STR_ZOOM "Mouse wheel or +/-: zoom"
# define STR_JULIAOPTIONS "Q/A/W/S: change JULIA constant manually"
# define STR_JULIAMOUSE "Mouse dependency (E to change):"

void	draw_current_set(t_win *win);
void	set_current_set(t_win *win, enum e_currentset index);
int		iterations(t_dpair *c, t_dpair *h);

#endif
