/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_tools.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:51:44 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:51:48 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DRAW_TOOLS_H
# define DRAW_TOOLS_H

# include "main.h"

# define CLR_WHITE 0xFFFFFF
# define CLR_BLACK 0x000000
# define PALETTE_A 1
# define PALETTE_B 2
# define PALETTE_C 3

void	main_palette_init(t_win *win);
void	draw_pixel(t_win *win, int x, int y, int iterations);

#endif
