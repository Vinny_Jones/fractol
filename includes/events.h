/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:51:59 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:52:02 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVENTS_H
# define EVENTS_H

# include "main.h"
# include "draw_tools.h"
# include "tools_set.h"
# include "set_julia.h"

# define ESC_KEY 53
# define ZOOM_IN_KEY 5
# define ZOOM_OUT_KEY 4

# define UP_KEY 126
# define DOWN_KEY 125
# define LEFT_KEY 123
# define RIGHT_KEY 124
# define PLUS_KEY1 24
# define PLUS_KEY2 69
# define MINUS_KEY1 27
# define MINUS_KEY2 78
# define MENU_KEY 46

# define SET_1 18
# define SET_2 19
# define SET_3 20
# define SET_4 21
# define SET_5 23

# define ZOOM_IN 0
# define ZOOM_OUT 1

# define PALETTE_A_KEY 83
# define PALETTE_B_KEY 84
# define PALETTE_C_KEY 85

int		manage_key_event(int key_code, void *param);
int		manage_mouse_event(int key_kode, int x, int y, void *param);
int		manage_julia_event(int x, int y, void *param);

#endif
