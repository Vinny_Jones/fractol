/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_celtic_five_mbar.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:52:27 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:52:29 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_CELTIC_FIVE_MBAR_H
# define SET_CELTIC_FIVE_MBAR_H

# include "main.h"
# include "draw_tools.h"
# include "tools_set.h"

void	init_celtic_5mbar(t_set *set);
void	*draw_celtic_5mbar(void *input);

#endif
