/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_mandelbar.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/27 12:52:49 by apyvovar          #+#    #+#             */
/*   Updated: 2018/10/27 12:52:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SET_MANDELBAR_H
# define SET_MANDELBAR_H

# include "main.h"
# include "draw_tools.h"
# include "tools_set.h"

void	init_mandelbar(t_set *set);
void	*draw_mandelbar(void *input);

#endif
