/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pairs.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:40:46 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:40:47 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PAIRS_H
# define PAIRS_H

typedef struct	s_ipair {
	int	x;
	int y;
}				t_ipair;

typedef struct	s_dpair {
	double	x;
	double	y;
}				t_dpair;

typedef struct	s_ldpair {
	long double	x;
	long double	y;
}				t_ldpair;

typedef struct	s_spair {
	size_t	x;
	size_t	y;
}				t_spair;

t_ipair			make_ipair(int x, int y);
t_dpair			make_dpair(double x, double y);
t_ldpair		make_ldpair(long double x, long double y);
t_spair			make_spair(size_t x, size_t y);

#endif
