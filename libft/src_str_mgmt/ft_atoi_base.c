/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/12 11:45:01 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/28 17:46:34 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	base_conv(const char *str, int base, int len)
{
	char	*basearr;
	int		i;
	int		res;
	int		mod;

	basearr = "0123456789ABCDEF";
	res = 0;
	mod = 1;
	while (--len >= 0)
	{
		i = -1;
		while (basearr[++i])
			if (basearr[i] == ft_toupper(str[len]))
			{
				res = res + mod * i;
				break ;
			}
			else if (i == base - 1)
				return (0);
		mod *= base;
	}
	return (res);
}

int			ft_atoi_base(const char *str, int base)
{
	int		sign;
	int		len;

	while (ft_isspace(*str))
		str++;
	sign = (*str == '-') ? -1 : 1;
	if (*str == '+' || *str == '-')
		str++;
	if (base <= 0 || base > 16 || !ft_ishex(*str))
		return (0);
	len = 0;
	while (ft_ishex(str[len]))
		len++;
	return (base_conv(str, base, len) * sign);
}
