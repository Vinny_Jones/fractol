/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 18:22:28 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	str_count(const char *str, char c)
{
	int	res;

	res = 0;
	while (*str)
	{
		while (*str == c)
			str++;
		if (*str)
			res++;
		while (*str && *str != c)
			str++;
	}
	return (res);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**result;
	size_t	i;
	size_t	j;
	size_t	len;

	if (!s)
		return (NULL);
	len = str_count(s, c);
	result = (char **)malloc(sizeof(char *) * (len + 1));
	if (!result)
		return (NULL);
	i = 0;
	while (i < len)
	{
		j = 0;
		while (*s == c)
			s++;
		while (*(s + j) && *(s + j) != c)
			j++;
		result[i] = ft_strsub(s, 0, j);
		s += j;
		i++;
	}
	result[i] = NULL;
	return (result);
}
