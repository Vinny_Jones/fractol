/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_f.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 15:35:45 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/20 12:40:02 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int			print_double(va_list ap, char *fmt, int width, int prec)
{
	char	*result;
	int		count;

	result = save_double(ap, fmt, fmt_isnumarg(fmt), prec);
	if ((CFMT('+') || CFMT(' ')) && *result != '-')
		apply_sign_flag(fmt, &result);
	apply_flwid_int(width, fmt, &result, prec);
	count = ft_strlen(result);
	ft_putstr(result);
	free(result);
	return (count);
}

char		*save_double(va_list ap, char *fmt, int num_arg, int prec)
{
	long double	nbr;

	if (num_arg)
		if (CFMT('L'))
			nbr = getnumarg_double(ap, ft_atoi(fmt + 1), 1);
		else
			nbr = getnumarg_double(ap, ft_atoi(fmt + 1), 0);
	else if (CFMT('L'))
		nbr = va_arg(ap, long double);
	else
		nbr = (long double)va_arg(ap, double);
	return (ft_ftoa(fmt, nbr, (CFMT('.') && prec >= 0) ? prec : 6));
}

long double	getnumarg_double(va_list ap, int n, int l_flag)
{
	long double	result;

	if (l_flag)
	{
		while (--n)
			va_arg(ap, intmax_t);
		result = va_arg(ap, long double);
		return (result);
	}
	while (--n)
		va_arg(ap, intmax_t);
	result = (long double)va_arg(ap, double);
	return (result);
}

char		*ft_ftoa(char *fmt, long double n, int prec)
{
	char	*result;
	int		i;

	result = ft_strnew(1000);
	i = 0;
	if (n < 0)
	{
		result[i++] = '-';
		n = n * -1;
	}
	save_dpart(&result[ft_strlen(result)], (unsigned long long int)n, 0);
	if (CFMT('\''))
		apply_thousand_sep(&result, ft_strlen(result));
	if (prec || CFMT('#'))
		ft_strcat(result, ".");
	n = n - (unsigned long long int)n;
	i = prec;
	while (i-- > 0)
		n *= 10;
	n = (((int)(n * 10)) % 10 > 4) ? n + 1 : n;
	if (prec > 0)
		save_dpart(&result[ft_strlen(result)], ABS(n), prec);
	return (result);
}

void		save_dpart(char *result, unsigned long long int n, int prec)
{
	int		i;
	int		len;

	i = 0;
	result[i++] = n % 10 + '0';
	while ((int)(n /= 10) && i < 1000)
		result[i++] = n % 10 + '0';
	len = ft_strlen(result);
	if (prec)
		while (prec-- - len > 0)
			ft_strcat(result, "0");
	ft_strrev(result);
}
