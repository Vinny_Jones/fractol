/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format_mgmt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/14 12:52:15 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/21 13:35:40 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		fmt_len(char *fmt)
{
	char	*temp;

	temp = fmt + ((fmt_isnumarg(fmt)) ? fmt_isnumarg(fmt) : 1);
	while (*temp && !ft_strchr(CONV, *temp) && ft_strchr(SKIP, *temp))
		if (fmt_isnumarg(temp))
			temp += fmt_isnumarg(temp);
		else
			temp++;
	return (temp - fmt + 1);
}

int		fmt_isnumarg(char *fmt)
{
	char *temp;

	if (*fmt == '%' || *fmt == '*')
		temp = fmt + 1;
	else
		return (0);
	if (ft_isdigit(*temp))
		while (ft_isdigit(*temp))
			temp++;
	else
		return (0);
	if (*temp == '$')
		return (temp - fmt + 1);
	else
		return (0);
}

void	fmt_fin(va_list ap_f, va_list ap, char *fmt, int count)
{
	int len;
	int	i;
	int skip;
	int	maxnum;

	i = 0;
	skip = 0;
	maxnum = 0;
	if (fmt_isnumarg(fmt))
	{
		maxnum = ft_atoi(fmt + 1);
		while (fmt[i])
		{
			if (fmt[i] == '*' && !fmt_isnumarg(&fmt[i]))
				skip++;
			i++;
		}
	}
	while (skip++ < maxnum - 1)
		va_arg(ap, uintmax_t);
	if (skip <= maxnum)
		ACFMT('f') ? va_arg(ap, double) : va_arg(ap, uintmax_t);
	len = ft_strlen(fmt) - 1;
	if (fmt[len] == 'n')
		print_nspec(ap_f, ap, fmt, count);
}
