/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wstr.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/06 19:23:03 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/21 11:34:22 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		print_wstr(wchar_t *str, int width, int prec, char *fmt)
{
	int		bytes;
	int		j;
	char	filler;

	if (!str)
		str = L"(null)";
	bytes = wstr_len(str, prec);
	filler = isnulflag(fmt) ? '0' : ' ';
	j = 0;
	while (!CFMT('-') && j++ < width - bytes)
		write(1, &filler, 1);
	j = 0;
	while (j < bytes)
		j += write_wchar(*str++, "", 0);
	j = 0;
	while ((CFMT('-') || width < 0) && j++ < ABS(width) - bytes)
		write(1, " ", 1);
	bytes += ((ABS(width) - bytes > 0) ? ABS(width) - bytes : 0);
	return (bytes);
}

int		wstr_len(wchar_t *str, int prec)
{
	int bytes;
	int	i;
	int	j;

	bytes = 0;
	i = -1;
	j = 1;
	while (str[++i] && j)
		if (str[i] <= 127)
			(bytes + 1 <= prec || prec < 0) ? bytes += 1 : j--;
		else if (str[i] <= 2047)
			(bytes + 2 <= prec || prec < 0) ? bytes += 2 : j--;
		else if (str[i] <= 65535)
			(bytes + 3 <= prec || prec < 0) ? bytes += 3 : j--;
		else if (str[i] <= 1114111)
			(bytes + 4 <= prec || prec < 0) ? bytes += 4 : j--;
	return (bytes);
}

wchar_t	*get_wstr(va_list ap, int num_arg, char *fmt)
{
	wchar_t	*res;
	int		n_arg;

	if (num_arg)
	{
		n_arg = ft_atoi(fmt + 1);
		while (--n_arg)
			va_arg(ap, wchar_t*);
		res = va_arg(ap, wchar_t*);
	}
	else
		res = va_arg(ap, wchar_t*);
	return (res);
}
