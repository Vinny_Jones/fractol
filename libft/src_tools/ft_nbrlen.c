/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 23:40:35 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/22 23:40:36 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_nbrlen(ssize_t nbr)
{
	int result;

	result = (nbr < 0) ? 2 : 1;
	while ((nbr /= 10))
		result++;
	return (result);
}
