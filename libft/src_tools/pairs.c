/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pairs.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/14 21:41:18 by apyvovar          #+#    #+#             */
/*   Updated: 2018/03/14 21:41:18 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_ipair		make_ipair(int x, int y)
{
	t_ipair	result;

	result.x = x;
	result.y = y;
	return (result);
}

t_dpair		make_dpair(double x, double y)
{
	t_dpair	result;

	result.x = x;
	result.y = y;
	return (result);
}

t_ldpair	make_ldpair(long double x, long double y)
{
	t_ldpair	result;

	result.x = x;
	result.y = y;
	return (result);
}

t_spair		make_spair(size_t x, size_t y)
{
	t_spair result;

	result.x = x;
	result.y = y;
	return (result);
}
